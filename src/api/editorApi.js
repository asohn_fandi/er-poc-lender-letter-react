import { handleResponse, handleError } from "./apiUtils";
const baseUrl = process.env.REACT_APP_API_URL + "/form/";

export function getHtml() {
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}

export function saveHtml(html) {
  return fetch(baseUrl, {
    method: "PUT", // POST for create, PUT to update when id already exists.
    headers: { "content-type": "application/json" },
    body: JSON.stringify(html)
  })
    .then(handleResponse)
    .catch(handleError);
}

export function deleteHtml(html) {
  return fetch(baseUrl, { method: "DELETE" })
    .then(handleResponse)
    .catch(handleError);
}
