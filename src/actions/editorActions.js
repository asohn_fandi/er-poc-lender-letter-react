import dispatcher from "../appDispatcher";
import actionTypes from "./actionTypes";
//import * as editorApi from "../api/editorApi";

let _html = "<p>Hello from CKEditor 5! Decoupled Editor test 2</p>";

export function saveHtml(Html) {
  //return editorApi.saveHtml(Html).then(saveHtml => {
  _html = Html;
  dispatcher.dispatch({
    actionTypes: actionTypes.CREATE_HTML,
    Html: Html
  });
  //});
}

export function loadHtml() {
  dispatcher.dispatch({
    actionTypes: actionTypes.LOAD_HTML,
    Html: _html
  });
}
