import { EventEmitter } from "events";
import Dispatcher from "../appDispatcher";
import actionTypes from "../actions/actionTypes";

const CHANGE_EVENT = "change";
let _html = "";

class EditorStore extends EventEmitter {
  addChangeListener(callback) {
    this.on(CHANGE_EVENT, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

  emitChange() {
    this.emit(CHANGE_EVENT);
  }

  getHtml() {
    return _html;
  }
}

const store = new EditorStore();

Dispatcher.register(action => {
  console.log("dispatcher was called. " + action.Html);
  console.log("action.actionType. " + action.actionType);
  switch (action.actionType) {
    case actionTypes.CREATE_HTML:
      console.log("Create dispatcher was called. " + action.Html);
      _html = action.Html;
      store.emitChange();
      break;
    case actionTypes.LOAD_HTML:
      console.log("Load dispatcher was called. " + action.Html);
      _html = action.Html;
      store.emitChange();
      break;
    default:
    //nothing to do here
  }
});

export default store;
