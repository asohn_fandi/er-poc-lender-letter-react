import LetterVariableEditing from './lettervariableediting';
import LetterVariableUI from './lettervariableui';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin'

export default class LetterVariable extends Plugin {
    static get requires() {
        return [ LetterVariableEditing, LetterVariableUI ];
    }
}