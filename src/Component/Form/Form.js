import React, { useState, useEffect } from "react";
import CKEditor from "@ckeditor/ckeditor5-react";
//import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import DecoupledEditor from "@ckeditor/ckeditor5-build-decoupled-document";
import editorStore from "../../stores/editorStore";
import * as editorActions from "../../actions/editorActions";
import { toast } from "react-toastify";
import styles from "./Form.module.css";
import "./CKEditor.css";
//import LetterVariable from "./CKEditor-Plugins/lettervariable/lettervariable";

const Form = props => {
  const [htmlData, setHtmlData] = useState(props.html);

  useEffect(() => {
    editorStore.addChangeListener(onChange);
    console.log("the html = " + editorStore.getHtml());
    if (editorStore.getHtml().length === 0) editorActions.loadHtml();
    //setHtmlData(editorStore.getHtml());
    //editorActions.loadHtml(editorStore.getHtml());
    return () => {
      editorStore.removeChangeListener(onChange);
    };
  }, []);

  const onChange = () => {
    setHtmlData(editorStore.getHtml());
  };

  const loadDefault = event => {
      // eslint-disable-next-line
    const defaultForm = "<div style='width:720px; font-family: arial, Sans-Serif; font-size: 12px;'><table border='0'><tbody><tr><td style='vertical-align: top; width:500px;'>{lenderLogo}</td><td style='font-size: 12px; text-align: left;'><span>{lenderName}<br />{lenderDepartment}<br />Phone: {lenderPhone}<br />Fax: {lenderFax}</span></td></tr></tbody></table><p style='font-size: 14px; margin: 0; text-align: left;'>{currentDate}</p><table border='0' style='margin: 0 auto; width: 100%;'><tbody><tr><td> <table cellspacing='0' cellpadding='0' style='font-size: 12px; margin: 0 auto; text-align: left'><tbody><tr><td><span style='text-decoration: underline;'><strong>Originating Dealership</strong></span></td></tr><tr><td>DLR #{dealerNumber} {dealerName}</td></tr><tr><td>{dealerAddress}</td></tr><tr><td>{dealerCity}, {dealerState} {dealerPostal}</td></tr><tr><td>Attn: Business Office/F&I Director</td></tr></tbody></table></td><td><table cellspacing='0' cellpadding='0' style='font-size: 12px; margin: 0 auto; text-align: left'><tbody><tr><td><span style='text-decoration: underline;'><strong>CC: F&I Product Provider</strong></span></td></tr><tr><td>{providerName}</td></tr><tr><td>{providerAddress}</td></tr><tr><td>{providerCity}, {providerState} {providerPostal}</td></tr><tr><td>Attn: Cancellations</td></tr></tbody></table></td></tbody></table><br /><h3 style='text-align: center;'>LENDER-INITIATED PRODUCT CANCELLATION NOTICE</h3><p>On {cancelDate} {lenderName} closed the loan on the vehicle referenced below due to <strong>{cancelType}</strong>.Therefore, we are submitting this request to cancel the product described below that was financed in the contract.Please note that there are <strong>actions required</strong> for<strong>both <span style='text-decoration: underline;'>Provider</span> and <span style='text-decoration: underline;'>Dealer</span></strong>.</p><table cellpadding='0' cellspacing='0' style='width: 100%;'><thead style='background-color: #000; border-right: 1px solid #000; margin: 0; padding: 0;'><tr><th colspan='3' style='color: #fff; font-size: 14px; font-weight: normal; padding: 3px; text-align: left;'>Customer Information</th></tr></thead><tbody><tr><td style='border: 1px solid #000; border-right: 0; padding-bottom: 5px; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Customer Name</span><br /><span style='font-size: 11px;'>{customerName}</span></td><td style='border: 1px solid #000; border-right: 0; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Vehicle Year Make Model</span><br /><span style='font-size: 11px;'>{vehicleYear} {vehicleMake} {vehicleModel}</span></td><td style='border: 1px solid #000; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>VIN</span><br /><span style='font-size: 11px;'>{vehicleVIN}</span></td></tr></tbody></table><br /><table cellpadding='0' cellspacing='0' style='width: 100%;'><thead style='background-color: #000; border-right: 1px solid #000; margin: 0; padding: 0;'><tr><th colspan='3' style='color: #fff; font-size: 14px; font-weight: normal; padding: 3px; text-align: left;'>Product Information</th></tr></thead><tbody><tr><td style='border: 1px solid #000; border-right: 0; padding-bottom: 5px; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Cancellation Type</span><br /><span style='font-size: 11px;'>{cancelType}</span></td><td style='border: 1px solid #000; border-right: 0; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Product Type</span><br /><span style='font-size: 11px;'>{productText}</span></td><td style='border: 1px solid #000; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Retail Price</span><br /><span style='font-size: 11px;'>${contractPrice}</span></td></tr><tr><td style='border: 1px solid #000; border-right: 0; border-top: 0; padding-bottom: 5px; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Cancellation Date</span><br /><span style='font-size: 11px;'>{cancelDate}</span></td><td style='border: 1px solid #000; border-right: 0; border-top: 0; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Cancellation Mileage</span><br /><span style='font-size: 11px;'>{vehicleMileage}</span></td><td style='border: 1px solid #000; border-top: 0; padding-left: 5px; vertical-align: top; width: 33%;'><span style='font-size: 11px;'>Contract Effective Date</span><br /><span style='font-size: 11px;'>{contractDate}</span></td></tr></tbody></table><br /><table cellpadding='0' cellspacing='0' style='width: 100%;'><thead style='background-color: #000; border-right: 1px solid #000; margin: 0; padding: 0;'><tr><th colspan='3' style='color: #fff; font-size: 14px; font-weight: normal; padding: 3px; text-align: left;'>Notice to Provider</th></tr></thead><tbody><tr><td style='border: 1px solid #000; border-right: 0; font-size: 12px; padding: 5px; vertical-align: top; width: 100%;'><strong>Please cancel the product(s) listed above and send your portion of the refund to the dealer.</strong></td><td style='border: 1px solid #000; border-left: 0; border-right: 0;'>&nbsp;</td><td style='border: 1px solid #000; border-left: 0;'>&nbsp;</td></tr></tbody></table><br /><table cellpadding='0' cellspacing='0' style='width: 100%;'><thead style='background-color: #000; border-right: 1px solid #000; margin: 0; padding: 0;'><tr><th colspan='3' style='color: #fff; font-size: 14px; font-weight: normal; padding: 3px; text-align: left;'>Notice to Dealer</th></tr></thead><tbody><tr><td style='border: 1px solid #000; border-right: 0; font-size: 12px; padding: 0 5px; vertical-align: top;'><p><strong>Issue the cancellation refund due to {lenderName}. DO NOT ISSUE A REFUND TO THE CONSUMER.</strong></p><p><strong>Please note - </strong>If the reimbursements are not received within 60 days of the date of this notice,refund amounts not reimbursed may be deducted from your dealer reserve account.</p><p><strong>Remit the cancellation refund amount to:</strong></p><p style='padding-left: 45px;'>{lenderName}<br/>{lenderAddress}<br />{lenderCity}, {lenderState} {lenderPostal}<br />Attn: {lenderDepartment}<br/></p><p>Please include the customer name, VIN and product type (GAP, Credit Life, Accident & Health, VSC, etc.) in the memo field of the check to ensure that the refund is applied correctly. </p><p>If the refund was applied to a car deal, please provide evidence of the refund by emailing us at {lenderEmail}. </p></td><td style='border: 1px solid #000; border-left: 0; border-right: 0;'>&nbsp;</td><td style='border: 1px solid #000; border-left: 0;'>&nbsp;</td></tr></tbody></table><p>Should you have any questions regarding this notice, please call us at {lenderPhone}.</p><p>Thank you,</p><p>{lenderName}</p></div>";

    setHtmlData(defaultForm);
  };
  const handleSubmit = event => {
    event.preventDefault();
    props.onHtmlUpdate(htmlData);

    editorActions.saveHtml(htmlData);
    props.history.push("/");
    toast.success("Form Saved.");
  };

  return (
    <>
      <div
        className={styles.button}
        type="submit"
        value="Submit"
        size="145"
        onClick={loadDefault}
      >
        Default Form
      </div>
      <div
          className={styles.button}
          type="submit"
          value="Submit"
          size="145"
          onClick={handleSubmit}
      >
          Save
      </div>
      <div className={styles.CKEditor}>
        <CKEditor
          editor={DecoupledEditor}
          data={htmlData}
          onInit={editor => {
              console.log('editor', editor);
            editor.ui
              .getEditableElement()
              .parentElement.insertBefore(
                editor.ui.view.toolbar.element,
                editor.ui.getEditableElement()
              );
          }}
          onChange={(event, editor) => {
            const data = editor.getData();
            setHtmlData(data);
          }}
          onBlur={(event, editor) => {
            setHtmlData(editor.getData());
          }}
          onFocus={(event, editor) => {
            console.log("Focus.", editor);
          }}
          config={{
            //plugins: [ LetterVariable ],
            toolbar: [ /*'save-letter', '|',*/ 'Undo', 'Redo', '|', 'heading', '|', 'FontSize', '|',  'bold', 'italic', 'underline', '|'/*, 'lettervariable'*/, '|', 'insertTable']
              // TODO: "Save" and "Revert to Default" buttons
              // TODO: plugin for horizontal rule - https://ckeditor.com/cke4/addon/lettervariable
              // TODO: dropdown list for {dealershipName} variables - https://ckeditor.com/cke4/addon/strinsert
              // TODO: better table toolbar plugin, native one kinda sucks - https://ckeditor.com/cke4/addon/tabletoolstoolbar maybe?
          }}
        />
      </div>
    </>
  );
};

export default Form;
