// import React, { useEffect } from "react";
// import DecoupledEditor from "@ckeditor/ckeditor5-build-classic";
// import PropTypes from "prop-types";

// const CKEditor = props => {
//   const { data, onChange, config } = props;
//   let editor = null;
//   let domContainer = null;

//   useEffect(() => {
//     _initializeEditor();
//     return () => {
//       _destroyEditor();
//     };
//   }, []);

//   useEffect(() => {
//     editor.setData(data);
//   }, [data]);

//   const _initializeEditor = () => {
//     DecoupledEditor.create(domContainer, config)
//       .then(e => {
//         // TODO: set default prop types for data and onChange.
//         editor = e;
//         editor.setData(data);
//         editor.model.document.on("change", () => onChange(e.getData()));
//       })
//       .catch(error => {
//         console.error(error);
//       });
//   };

//   const _destroyEditor = () => {
//     if (editor) {
//       editor.destroy();
//     }
//   };

//   return <div ref={ref => (domContainer = ref)}></div>;
// };

// CKEditor.propTypes = {
//   editor: PropTypes.func.isRequired,
//   data: PropTypes.string,
//   config: PropTypes.object,
//   onChange: PropTypes.func,
//   onInit: PropTypes.func,
//   onFocus: PropTypes.func,
//   onBlur: PropTypes.func,
//   disabled: PropTypes.bool
// };

// CKEditor.defaultProps = {
//   config: {}
// };

// export default React.memo(CKEditor);
