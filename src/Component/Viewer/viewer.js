import React, { useEffect } from "react";
import editorlStore from "../../stores/editorStore";
import { loadHtml } from "../../actions/editorActions";

const Home = props => {
  useEffect(() => {
    if (editorlStore.getHtml().length === 0) loadHtml();
  }, []);

  return <>{props.html}</>;
};

export default Home;
