import React from "react";
import { NavLink } from "react-router-dom";

const home = () => {
  const activeStyle = { color: "gray" };
  return (
    <nav>
      <ul>
        <li>
          <NavLink activeStyle={activeStyle} exact to="/">
            Home
          </NavLink>
        </li>
        <li>
          <NavLink activeStyle={activeStyle} to="/form">
            Html Editor
          </NavLink>
        </li>
        <li>
          <NavLink activeStyle={activeStyle} to="/viewer">
            Html Viewer
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default home;
