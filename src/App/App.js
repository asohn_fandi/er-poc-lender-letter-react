import React, { useState } from "react";
import logo from "../logo.svg";
import "./App.css";
import Form from "../Component/Form/Form";
import Home from "../Component/Home/Home";
import Viewer from "../Component/Viewer/viewer";
import { Route } from "react-router-dom";
import Nav from "./nav";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  const [html, setHtml] = useState(
    "<p>Hello from CKEditor 5! Decoupled Editor</p>"
  );

  const handelHtmlUpdate = htmlData => {
    setHtml(htmlData);
  };
  return (
    <div className="App">
      <ToastContainer autoClose={3000} hideProgressBar />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Input Components</p>
      </header>
      <Nav />
      <div className="body">
        <Route path="/" exact component={Home} />
        <Route
          path="/form"
          exact
          render={props => (
            <Form {...props} onHtmlUpdate={handelHtmlUpdate} html={html} />
          )}
        />
        <Route
          path="/viewer"
          exact
          render={props => <Viewer {...props} html={html} />}
        />
      </div>
    </div>
  );
}

export default App;
